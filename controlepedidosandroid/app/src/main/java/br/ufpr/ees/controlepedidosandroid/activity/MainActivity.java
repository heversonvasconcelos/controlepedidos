package br.ufpr.ees.controlepedidosandroid.activity;

import android.content.Intent;
import android.ees.ufpr.br.controlepedidosandroid.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case R.id.cliente:
                startActivity(new Intent(this, ClientesActivity.class));
                return true;
            case R.id.produto:
                // chamar tela de produto
                return true;
            case R.id.pedido:
                // chamar tela de pedido
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }
    }
}
