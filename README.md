# Controle de Pedidos

## Descrição

Projeto para controlar o processo de pedido de produtos

## Sub-projetos

* controlepedidos
  * controlepedidosbackend (Projeto backend)
    * controlepedidosdomain (Camada de domínio)
    * controlepedidosws (Camada de webservice)
  * controlepedidosweb (Projeto web)
  * controlepedidosandroid (Projeto android)

## Tecnologias e frameworks

## Como executar

//TODO

## Equipe

* Heverson Silva Vasconcelos <heverson.vasconcelos@gmail.com>
* Luis Alberto de Quadros <luislaq@gmail.com>
* Diogo Alexandro Pereira <diogoalexp@gmail.com>
