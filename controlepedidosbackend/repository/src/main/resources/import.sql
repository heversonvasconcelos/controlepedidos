---TABELA CLIENTE
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-1, '82725040060', 'Joao', 'Mario Silva', 'joaojms@gmail.com');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-2, '56014221093', 'Francisco', 'Gomes', 'franciscofg@hotmail.com');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-3, '67449254077', 'Luis', 'Augusto', 'luisaugusto@email.com');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-4, '16850970062', 'Maria', 'Pereira', 'mariamp@uol.com.br');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-5, '20472254006', 'Julia', 'Joaquina', 'jjoaquina@gmail.com');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-6, '36654325028', 'Carlos', 'Alberto', 'carlos.alberto@outlook.com');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-7, '71805257072', 'Mario', 'Joao Bandeiras', 'bandeirasmjb@bigmail.com');
INSERT INTO CLIENTE(id, cpf, nome, sobrenome, email) VALUES (-8, '23474920049', 'Jardel', 'Oliveira Filho', 'oliveirajof@outlook.com');

---TABELA PRODUTO
INSERT INTO PRODUTO(id, descricao) VALUES (-1, 'Papel A4 Chamex');
INSERT INTO PRODUTO(id, descricao) VALUES (-2, 'Caneta Esferografica Azul Bic');
INSERT INTO PRODUTO(id, descricao) VALUES (-3, 'Pasta Polibras');
INSERT INTO PRODUTO(id, descricao) VALUES (-4, 'Lapis Faber Castell');
INSERT INTO PRODUTO(id, descricao) VALUES (-5, 'Grampeador CiS');
INSERT INTO PRODUTO(id, descricao) VALUES (-6, 'Agenda Tilibra');
INSERT INTO PRODUTO(id, descricao) VALUES (-7, 'Regua 30cm Trident');
INSERT INTO PRODUTO(id, descricao) VALUES (-8, 'Calculadora Cientifica Casio');

---TABELA PEDIDO
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-1, '2011-01-12 00:00:06', -1);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-2, '2012-02-01 01:11:05', -2);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-3, '2013-03-02 02:22:04', -3);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-4, '2014-04-03 03:33:03', -4);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-5, '2015-05-04 04:44:02', -5);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-6, '2016-06-05 05:55:01', -6);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-7, '2017-07-06 06:17:05', -1);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-8, '2017-08-07 07:18:06', -2);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-9, '2001-09-08 08:19:07', -1);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-10, '2002-10-09 09:20:08', -2);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-11, '2003-11-10 10:21:09', -3);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-12, '2004-12-11 11:22:10', -4);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-13, '2001-01-01 12:23:11', -5);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-14, '2001-02-02 13:24:12', -6);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-15, '2001-03-03 14:25:13', -1);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-16, '2001-04-04 15:26:14', -2);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-17, '2001-05-05 16:27:15', -1);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-18, '2001-06-06 17:28:16', -2);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-19, '2001-07-07 18:29:17', -3);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-20, '2001-08-08 19:30:50', -4);
INSERT INTO PEDIDO(id, data_pedido, cliente_fk) VALUES (-21, '2001-09-09 20:31:52', -5);

---TABELA ITEM PEDIDO
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-1, 10, -1, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-2, 5, -1, -2);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-3, 1, -1, -3);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-4, 4, -1, -4);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-5, 1, -1, -5);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-6, 2, -1, -6);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-7, 1, -1, -7);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-8, 1, -1, -8);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-9, 1, -2, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-10, 2, -3, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-11, 3, -4, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-12, 4, -5, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-13, 5, -6, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-14, 6, -7, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-15, 7, -8, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-16, 8, -9, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-17, 9, -10, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-18, 10, -11, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-19, 11, -12, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-20, 12, -13, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-21, 13, -14, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-22, 14, -15, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-23, 15, -16, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-24, 16, -17, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-25, 17, -18, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-26, 18, -19, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-27, 19, -20, -1);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-28, 20, -11, -5);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-29, 21, -11, -6);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-30, 22, -11, -7);
INSERT INTO ITEM_PEDIDO(id, quantidade, pedido_fk, produto_fk) VALUES (-31, 320, -21, -1);


